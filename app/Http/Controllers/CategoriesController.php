<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin'])->only('edit', 'update', 'trash', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(10);
        //compact: as in view it is used to take array for var/object which is returned by DB and
        //passed as value to key categories which is obj/var in view, so to avoid of key => value in
        //array, we use compact() which takes var name and directly do key => value automatically.
        return view('categories.index', compact(['categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        // 1. Validation is already done with the help of Form request

        // 2. store the data in db
        Category::create([
            'name' => $request->name
        ]);

        //session
        session()->flash('success', 'Category created successfully');
        //3. return to index
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) //route-model binding
    {
        return view('categories.edit', compact(['category']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        //$category->update(['name' => '$request->name']);
        $category->save();

        //session
        session()->flash('success', 'Category Updated Successfully');

        //redirect
        return redirect(route('categories.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $categoryId)
    {
        $category = Category::onlyTrashed()->findOrFail($categoryId);

        if ($category->posts()->count() > 0) {
            session()->flash('error', 'This category cannot be deleted as it is associated with some post');
            return redirect(route('categories.index'));
        }
        $category->forceDelete();
        session()->flash('success', $category->name . ' category deleted successfully');
        return redirect(route('categories.index'));
    }

    public function trashed()
    {
        $categories = Category::onlyTrashed()->paginate(10);
        return view('categories.trashed', compact(['categories']));
    }

    public function trash(Category $category)
    {
        $category->delete();
        session()->flash('success', $category->name . ' trashed successfully');
        return redirect(route('categories.index'));
    }

    public function restore(int $categoryId)
    {
        $category = Category::onlyTrashed()->findOrFail($categoryId);
        $category->update();
        session()->flash('success', 'Category restored successfully');
        return redirect(route('categories.index'));
    }
}

