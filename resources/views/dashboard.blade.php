@extends('layouts.admin-panel.app')

@section('content')
<div class="card">
    <div class="card-body">
      <p>Logged In</p>
    </div>
</div>

@if(! auth()->user()->isAdmin())
    <div>
        <a href="{{ route('subscriptions.add')}}" class="btn btn-outline-primary mt-2 mb-3">Subscription</a>
    </div>
@endif
@endsection
