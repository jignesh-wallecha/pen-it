@extends('layouts.admin-panel.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title"><h2>Categories</h2></div>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td>
                                <form action="{{ route('categories.restore', $category->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-sm btn-warning" type="submit">Restore</button>
                                </form>
                                <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $category->id }})" data-toggle="modal" data-target="#deleteModal">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deleteModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="" method="POST" id="deleteCategoryForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    Are u sure, you want to delete this category?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-danger" >Delete Category</button>
                </div>
            </form>
          </div>
        </div>
    </div>

    <div class="mt-5">
        {{ $categories->links('vendor.pagination.bootstrap-4') }}
    </div>
@endsection

@section('page-level-scripts')
    <script>
        //alert('hello');
        function displayModal(categoryId) {
            var url = "/categories/" + categoryId;
            $("#deleteCategoryForm").attr('action', url);
        }
    </script>
@endsection

