<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;


class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create category
        $categoryNews = Category::create(['name' => 'News']);
        $categoryDesign = Category::create(['name' => 'Design']);
        $categoryTechnology = Category::create(['name' => 'Technology']);
        $categoryEngineering = Category::create(['name' => 'Engineering']);

        //create tag
        $tagDesign = Tag::create(['name' => 'design']);
        $tagTerrorism = Tag::create(['name' => 'terrorism']);
        $tagPeace = Tag::create(['name' => 'peace']);
        $tagCoding = Tag::create(['name' => 'coding']);

        //create posts
        $post1 = Post::create([
            'title' => 'Skip List Algorithm',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->sentence(rand(3, 7), true),
            'image' => 'images/posts/1.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post2 = Post::create([
            'title' => 'We relocated our office to HOME!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->sentence(rand(3, 7), true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post3 = Post::create([
            'title' => 'Floor Design of Palace',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->sentence(rand(3, 7), true),
            'image' => 'images/posts/3.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post4 = Post::create([
            'title' => 'India-Taliban talks',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->sentence(rand(3, 7), true),
            'image' => 'images/posts/4.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post5 = Post::create([
            'title' => 'Pakistan is Black listed by FATF',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->sentence(rand(3, 7), true),
            'image' => 'images/posts/5.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post6 = Post::create([
            'title' => 'Javascript',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->sentence(rand(3, 7), true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 6,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);


        //bridge between post and  tag
        $post1->tags()->attach([$tagCoding->id]);
        $post2->tags()->attach([$tagDesign->id]);
        $post3->tags()->attach([$tagDesign->id]);
        $post4->tags()->attach([$tagPeace->id]);
        $post5->tags()->attach([$tagTerrorism->id]);
        $post6->tags()->attach([$tagCoding->id, $tagDesign->id]);

    }
}
