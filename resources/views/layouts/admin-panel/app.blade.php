<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('admin-panel/css/bootstrap.min.css') }}" crossorigin="anonymous">

    @yield('page-level-styles')

    <title>Pen It</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="{{ route('dashboard') }}">Pen It Admin Panel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


       <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <ul class="navbar-nav ml-auto">
               <li class="nav-item dropdown">
                   <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       {{ auth()->user()->name }}
                   </a>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href=" {{route('users.edit', auth()->user()->id) }}" class="btn btn-primary">
                            Edit
                        </a>
                        <form action="{{ route('users.destroy', auth()->user()->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                       <form action="{{ route('logout') }}" method="POST">
                           @csrf
                           <input type="submit" class="dropdown-item" value="Logout">
                       </form>

                   </div>
               </li>
           </ul>
       </div>
   </div>
</nav>
<div class="container py-5">
    <div class="row">
        <div class="col-md-4">
           <div class="card">
               <div class="card-body">
                   <ul class="nav flex-column">
                       <li class="nav-item">
                           <a href="{{ route('dashboard') }}" class="nav-link">Dashboard</a>
                           <a href="{{ route('posts.index') }}" class="nav-link">Posts</a>
                           <a href="{{ route('categories.index') }}" class="nav-link">Categories</a>
                           <a href="{{ route('tags.index') }}" class="nav-link">Tags</a>
                           <hr>
                            @if(auth()->user()->isAdmin())
                                <a href="{{ route('blogs.index') }}" class="nav-link">Blogs</a>
                                <hr>
                                <a href="{{ route('subscriptions.index') }}" class="nav-link">Subscriptions</a>
                                <hr>
                            @endif
                           <a href="{{ route('users.index') }}" class="nav-link">Users</a>
                           <hr>
                           <a href="{{ route('posts.drafts') }}" class="nav-link">Drafts</a>
                           <hr>
                           <a href="{{ route('post.trashed') }}" class="nav-link">Trashed Post</a>
                           <hr>
                           <a href="{{ route('tags.trashed') }}" class="nav-link">Trashed Tags</a>
                           <hr>
                           <a href="{{ route('categories.trashed') }}" class="nav-link">Trashed Categories</a>
                           <hr>
                           <a href="{{ route('home') }}" class="nav-link">Visit Home Page</a>
                       </li>
                   </ul>
               </div>
           </div>
        </div>
        <div class="col-md-8">
            @include('layouts.partials._message')
            @yield('content')
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('admin-panel/js/bootstrap.min.js') }}"></script>

@yield('page-level-scripts')
</body>
</html>
