<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\SubscriptionsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('home');

Route::get('/help', fn() => view('blogs.help'))->name('help');

Route::get('/dashboard', fn() => view('dashboard'))
        ->middleware(['auth'])
        ->name('dashboard');

Route::get('/blogs/{post}', [FrontendController::class, 'show'])->name('blogs.show');
Route::get('/blogs/category/{category}', [FrontendController::class, 'category'])->name('blogs.category');
Route::get('/blogs/tags/{tag}', [FrontendController::class, 'tag'])->name('blogs.tag');


require __DIR__.'/auth.php';

//Application routes
//NOTE: have used raw url instead of categories.destroy

Route::middleware(['auth'])->group(function() {

    Route::delete('categories/{category}/trash', [CategoriesController::class, 'trash'])->name('categories.trash');
    Route::get('categories/trashed', [CategoriesController::class, 'trashed'])->name('categories.trashed');
    Route::put('categories/{category}/restore', [CategoriesController::class, 'restore'])->name('categories.restore');
    Route::resource('categories', CategoriesController::class);

    Route::delete('tags/{tag}/trash/', [TagsController::class, 'trash'])->name('tags.trash');
    Route::get('tags/trashed', [TagsController::class, 'trashed'])->name('tags.trashed');
    Route::put('tags/{tag}/restore', [TagsController::class, 'restore'])->name('tags.restore');
    Route::resource('tags', TagsController::class);

    Route::delete('posts/{post}/trash', [PostsController::class, 'trash'])->name('posts.trash');
    Route::get('posts/trashed', [PostsController::class, 'trashed'])->name('post.trashed');
    Route::put('posts/restore/{post}', [PostsController::class, 'restore'])->name('posts.restore');

    Route::get('posts/drafts/', [PostsController::class, 'draft'])->name('posts.drafts');
    Route::put('posts/{post}/publish', [PostsController::class, 'publish'])->name('posts.publish');
    Route::get('posts/{post}/preview', [PostsController::class, 'preview'])->name('posts.preview');

    Route::resource('posts', PostsController::class);


    //Subscription routes
    Route::get('subscriptions/add', [SubscriptionsController::class, 'add'])->name('subscriptions.add');
    Route::post('subscriptions', [SubscriptionsController::class, 'store'])->name('subscriptions.store');
    Route::delete('subscriptions/{subscription}/cancel', [SubscriptionsController::class, 'cancel'])->name('subscriptions.cancel');

    //Comments routes
    Route::post('posts/{post}/comments', [CommentsController::class, 'store'])->name('posts.comments.store');
    Route::put('posts/{post}/comments/{comment}/edit', [CommentsController::class, 'update'])->name('posts.comments.update');
    Route::get('posts/{post}/comments/{comment}', [CommentsController::class, 'reply'])->name('posts.comments.reply');
    Route::post('posts/{post}/comments/{comment}/reply', [CommentsController::class, 'replyStore'])->name('posts.comments.store.reply');
});

Route::middleware(['auth', 'admin'])->group(function() {
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::get('users/{user}/edit', [UsersController::class, 'edit'])->name('users.edit');
    Route::put('users/{user}', [UsersController::class, 'update'])->name('users.update');
    Route::delete('users/{user}', [UsersController::class, 'destroy'])->name('users.destroy');
    Route::put('/users/{user}/make-admin', [UsersController::class, 'makeAdmin'])->name('users.make-admin');
    Route::put('/users/{user}/revoke-admin', [UsersController::class, 'revokeAdmin'])->name('users.remove-admin');
    Route::put('posts/{post}/approve', [PostsController::class, 'approve'])->name('posts.approve');
    Route::put('posts/{post}/disapprove', [PostsController::class, 'disApprove'])->name('posts.disapprove');

    Route::get('/blogs', [PostsController::class, 'blogs'])->name('blogs.index');
    Route::get('/approved/blogs', [PostsController::class, 'approved'])->name('approved.blogs');
    Route::get('/disapproved/blogs', [PostsController::class, 'disapproved'])->name('disapproved.blogs');

    Route::get('/blogs/categories/{category}', [PostsController::class, 'showCategory'])->name('blogs.category');

    Route::get('/subscriptions', [SubscriptionsController::class, 'index'])->name('subscriptions.index');
});

Route::group([
    'prefix' => 'timepasschecking',
    'as' => 'timepasschecking.',
    'namespace' => "\App\Http\Controllers"
], function () {
    Route::get('/timepass', function () {
        return "hello timepass";
    });
});
