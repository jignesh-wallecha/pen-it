<?php

namespace Database\Seeders;

use App\Models\Plan;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //creating seeder for Plan table
        $monthly = Plan::create(['name' => 'monthly']);
        $quaterly = Plan::create(['name' => 'quaterly']);
        $halfYearly = Plan::create(['name' => 'half-yearly']);
        $annual  = Plan::create(['name' => 'annual']);

        //creating seeder for Subscription table
        Subscription::create([
            'user_id' => 1,
            'plan_id' => $monthly->id,
            'state' => 'active',
            'start_plan' => Carbon::now()->format('Y-m-d'),
            'expires_at' => Carbon::now()->addDays(30)
        ]);

        Subscription::create([
            'user_id' => 4,
            'plan_id' => $quaterly->id,
            'state' => 'active',
            'start_plan' => Carbon::now()->format('Y-m-d'),
            'expires_at' => Carbon::now()->addDays(90)
        ]);

        Subscription::create([
            'user_id' => 5,
            'plan_id' => $halfYearly->id,
            'state' => 'active',
            'start_plan' => Carbon::now()->format('Y-m-d'),
            'expires_at' => Carbon::now()->addDays(180)
        ]);

        Subscription::create([
            'user_id' => 6,
            'plan_id' => $annual->id,
            'state' => 'active',
            'start_plan' => Carbon::now()->format('Y-m-d'),
            'expires_at' => Carbon::now()->addDays(365)
        ]);



    }
}
