@extends('layouts.frontend.layout')

@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css">
@endsection

@section('header')
<header class="pt100 pb100 parallax-window-2" data-parallax="scroll" data-speed="0.5" data-image-src="{{ asset('frontend/assets/img/bg/img-bg-17.jpg')}}" data-positiony="1000">
    <div class="intro-body text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt50">
                    <h1 class="brand-heading font-montserrat text-uppercase color-light" data-in-effect="fadeInDown">
                        {{ $post->title }}
                        <small class="color-light alpha7">Articulated for You with &hearts;</small>
                    </h1>
                </div>
            </div>
        </div>

    </div>
</header>
@endsection

@section('main-content')
<div class="blog-three-mini" id="PostTime">
    <h2 class="color-dark"><a href="#">{{ $post->title }}</a></h2>
    <div class="blog-three-attrib">
        <div><i class="fa fa-calendar"></i>{{ $post->published_at->diffForHumans() }}</div> |
        <div><i class="fa fa-pencil"></i><a href="#">{{ $post->author->name}}</a></div> |
        <div id="readTime"></div> |
        <div><i class="fa fa-comment-o"></i><a href="#">90 Comments</a></div> |
        <div><a href="#"><i class="fa fa-thumbs-o-up"></i></a>150 Likes</div> |
        <div>
            Share:  <a href="#"><i class="fa fa-facebook-official"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
        </div>
    </div>

    <img src="{{ asset($post->image_path) }}" alt="Blog Image" class="img-responsive">

    <div>
        {!! $post->content !!}
    </div>

    <div class="blog-post-read-tag mt50">
        <i class="fa fa-tags"></i> Tags:
        @foreach ($post->tags as $tag)
            <a href="#">{{ $tag->name }}</a>{{ $post->last ? '' : ',' }}
        @endforeach
    </div>
</div>

<div class="blog-post-author mb50 pt30 bt-solid-1">
    <img src="{{ $post->author->gravatar_image  }}" width="80" alt="image">
    <span class="blog-post-author-name">{{ $post->author->name }}</span> <a href="https://twitter.com/booisme"><i class="fa fa-twitter"></i></a>
    <p>
        Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.
    </p>
</div>

@include('comments._create')
<hr>
@include('comments._index')

@endsection

@section('page-level-scripts')

    <script>

        function get_text(el) {
            ret = "";
            var length = el.childNodes.length;
            for (var i = 0; i < length; i++) {
                var node = el.childNodes[i];
                if (node.nodeType != 8) {
                    ret += node.nodeType != 1 ? node.nodeValue : get_text(node);
                }
            }
            return ret;
        }

        var words = get_text(document.getElementById('PostTime'));
        var countWords = words.split(/\s+/).length;
        const wordsPerMinute = 130;
        var readingTime = countWords / wordsPerMinute;
        readingTime = readingTime.toFixed(1);
        var readTime = readingTime + " min read";

        document.getElementById('readTime').innerHTML = readTime;
    </script>
    {{-- trix-editor cdn --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
@endsection
