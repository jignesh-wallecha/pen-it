@if ($paginator->hasPages())
    <div class="row mt25 animated" data-animation="fadeInUp" data-animation-delay="100">
        <div class="col-md-6">
            <a href="{{ $paginator->previousPageUrl() }}" class="button button-sm button-pasific pull-left hover-skew-backward
            @if($paginator->currentPage() <= 1)
                disabled btn btn-secondary
            @endif">
                Old Entries

            </a>
        </div>
        <div class="col-md-6">
            <a href="{{ $paginator->nextPageUrl() }}" class="button button-sm button-pasific pull-right hover-skew-forward
            @if (! $paginator->hasMorePages())
                disabled btn btn-secondary
            @endif">
            >
            {{-- style="background-color: gray !important" --}}
            New Entries</a>
        </div>
    </div>
@endif

