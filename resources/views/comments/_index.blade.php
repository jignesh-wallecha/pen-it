<div class="row mt-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="mt-0">{{ \Illuminate\Support\Str::plural('Comments', $post->comments_count) }}
                    @if($post->comments_count > 0)
                        ({{ $post->comments_count }})
                    @endif
                </h3>
            </div>
            <div class="card-body">
                @foreach ($post->comments as $comment)
                    <div class="d-flex flex-row">
                        <div>
                            <img src="{{ $comment->author->gravatar_image}}" alt="image" width="40" style="border-radius: 15px">
                            <span style="margin-left: 10px" class="font-weight-normal">{{ $comment->author->name }}</span>
                            <span class="align-items-end">{{ $comment->created_date }}</span>
                        </div>
                    </div>
                    <div style="margin-top: 10px; margin-bottom:10px">
                        {!! $comment->comment !!}
                    </div>
                    <div style="display: flex; justify-content: flex-end">
                        <a href="" class="btn btn-sm" id="reply">Reply</a>
                        <form action="{{ route('posts.comments.store.reply', [$post->id, $comment->id])}}">
                            <div class="form-group">
                                <input type="hidden" id="body" name="body" value="{{ old('body') }}">
                                <input type="hidden" name="comment_id" id="comment_id" value="{{ $comment->id}}">
                                <trix-editor input="body" class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}"></trix-editor>
                                @error('body')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Reply" class="btn btn-info">
                            </div>
                        </form>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>

@section('page-level-scripts')
    <script>
        function getReply() {
            document.getElementById('reply')
                    .addEventListener('click', )
        }
    </script>
@endsection
