<?php

namespace App\Http\Controllers;

use App\Http\Requests\Posts\CreatePostRequest;
use App\Http\Requests\Posts\UpdatePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['verifyCategoriesCount'])->only('create', 'store');
        $this->middleware(['verifyTagsCount'])->only('create', 'store');
        $this->middleware(['validateUser'])->only('edit', 'update', 'destroy', 'trash');
        $this->middleware(['validateUserPreview'])->only('preview');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->isAdmin())
        {
            $posts = Post::latest('published_at')->published()->paginate(3);
        } else {

            $posts = Post::where('user_id', auth()->id())->published()->paginate(3);
        }

        return view('posts.index', compact('posts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();

        return view('posts.create', compact(['categories', 'tags']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $image = $request->file('image')->store('images/posts');

        $post = Post::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'content' => $request->content,
            'category_id' => $request->category_id,
            'user_id' => auth()->id(),
            'image' => $image,
            'published_at' => $request->published_at
        ]);

        $post->tags()->attach($request->tags);

        session()->flash('success', 'Post Created Successfully');

        return redirect(route('posts.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();

        return view('posts.edit', compact(['post', 'categories', 'tags']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $data = $request->only('title', 'excerpt', 'content', 'published_at', 'category_id');

        if ($request->hasFile('image')) {
            $image = $request->image->store('images/posts');
            $data['image'] = $image;
            $post->deleteImage();
        }

        $post->update($data);

        $post->tags()->sync($request->tags);

        session()->flash('success', 'Post update successfully');

        return redirect(route('posts.index'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->deleteImage();
        $post->forceDelete();
        session()->flash('success', 'Post deleted successfully');
        return redirect(route('posts.index'));
    }

    public function trashed()
    {
        $trashed = Post::onlyTrashed()->paginate(10);
        return view('posts.trashed', ['posts' => $trashed]);
    }

    public function trash(Post $post)
    {
        $post->delete();
        session()->flash('success', 'Post Trashed');
        return redirect(route('posts.index'));
    }

    public function restore($id)
    {
        $trashedPost = Post::onlyTrashed()->findOrFail($id);
        $trashedPost->restore();
        session()->flash('success', 'Post restored successfully');
        return redirect(route('posts.index'));
    }

    public function draft()
    {
        $posts = Post::latest('published_at')->drafted()->paginate(3);

        return view('posts.draft', compact('posts'));
    }

    public function publish(int $postId)
    {
        $post = Post::findOrFail($postId);
        $post->update(['published_at' => now()]);
        session()->flash('success', $post->name . " published successfully");
        return redirect(route('posts.index'));

    }

    public function approve(Post $post)
    {
       // dd($post->author->name);
        $data = [
            'approved_at' => now(),
            'approver_id' => auth()->id(),
            'disapproved_at' => null,
            'disapprover_id' => null
        ];

        $post->update($data);

       session()->flash('success', $post->author->name . " posts approved successfully");
       return redirect(route('posts.index'));
    }

    public function disApprove(Request $request, Post $post)
    {

        $data = [
            'disapprover_id' => auth()->id(),
            'disapproved_at' => now(),
            'disapproved_status' => $request->message,
            'approved_at' => null,
            'approver_id' => null
        ];

        $post->update($data);

        session()->flash('success', $post->author->name . " posts disapproved successfully");
        return redirect(route('posts.index'));
    }

    public function preview(Post $post)
    {
        $tags = Tag::all();
        $categories = Category::all();

        return view('blogs.post', compact(['post', 'tags', 'categories']));
    }

    public function blogs()
    {
        $categories = Category::all();
        $blogs = Post::latest('published_at')->published()->paginate(3);
        return view('Blog.blog', ['posts' => $blogs, 'categories' => $categories]);
    }

    public function approved()
    {
        $categories = Category::all();
        $approvedPosts = Post::approved()->paginate(3);
        return view('Blog.approved', ['posts' => $approvedPosts, 'categories' => $categories]);
    }

    public function disapproved()
    {
        $categories = Category::all();
        $disapprovedPosts = Post::disapproved()->paginate(3);
        return view('Blog.disapproved', ['posts' => $disapprovedPosts, 'categories' => $categories]);
    }

    public function showCategory(int $categoryId)
    {
        $categories = Category::all();
        $singleCategory = Category::findOrFail($categoryId);
        //dd($category->name);
        return view('Blog.show', compact('singleCategory', 'categories'));
    }

}
