<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * Relationship methods
     */

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function isMonthly(): bool
    {
        return $this->name === 'monthly';
    }

    public function isQuaterly(): bool
    {
        return $this->name === 'quaterly';
    }

    public function isHalfYearly(): bool
    {
        return $this->name === 'half-yearly';
    }

    public function isAnnual(): bool
    {
        return $this->name = 'annual';
    }

}
