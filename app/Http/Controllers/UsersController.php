<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['validateUser'])->only('edit', 'update', 'destroy');
    }

    public function index()
    {
        $users = User::paginate(10);
        return view('users.index', compact(['users']));
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
       $data = $request->only('name', 'email', 'role');
       $user->update($data);
       session()->flash('success', $user->name . ' updated successfully');
       return redirect(route('users.index'));
    }


    public function destroy(User $user)
    {
        if ($user->posts()->withTrashed()->count() > 0) {
            session('error', $user->name . ' can\'t be deleted as with this user post is associated');
            return redirect(route('users.index'));
        }

        $user->delete();
        session()->flash('success', $user->name . ' user deleted successfully');
        return redirect(route('users.index'));
    }

    public function makeAdmin(User $user)
    {
        $user->update(['role' => User::ADMIN]);
        session()->flash('success', $user->name . ' has been asigned admin role');
        return redirect(route('users.index'));
    }

    public function revokeAdmin(User $user)
    {
        $user->update(['role' => User::AUTHOR]);
        session()->flash('success', $user->name . ' has been revoked from admin role');
        return redirect(route('users.index'));
    }
}
