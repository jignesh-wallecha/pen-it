<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateUserEditAndDelete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(is_object($request->user)) {
            if (! ($request->user->id == auth()->id())) {
                return redirect(abort(401));
            }
        } else if (is_numeric($request->user)) {
            if (! ($request->user == auth()->id)) {
                return redirect(abort(401));
            }
        }
        return $next($request);
    }
}
