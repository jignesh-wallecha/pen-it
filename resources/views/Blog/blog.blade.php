@extends('layouts.admin-panel.app')

@section('content')
    {{-- dropdown --}}
    <div class="btn-group postion-relative" style="bottom: 18px !important">
        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Latest
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="{{ route('approved.blogs')}}">Approved</a>
          <a class="dropdown-item" href="{{ route('disapproved.blogs') }}">Disapproved</a>
        </div>
    </div>

    <div class="btn-group postion-relative" style="top: -20px !important; left: 70px">
        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Category
        </button>
        <div class="dropdown-menu">
            @foreach ($categories as $category)
                <a class="dropdown-item" href="{{ route('blogs.category', $category->id)}}">{{ $category->name }}</a>
            @endforeach
        </div>
    </div>

    <div class="card">
        <div class="card-header"><h2>Posts</h2></div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Excerpt</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td><img src="{{ asset($post->image_path) }}" alt="" width="120"></td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->excerpt }}</td>
                            <td>
                                @if($post->isApproved())
                                    <div class="badge rounded-pill bg-primary">
                                        Approved
                                    </div>
                                @elseif($post->isDisapproved())
                                    <div class="badge rounded-pill bg-danger">
                                        Disapproved
                                    </div>
                                    <span class="text-danger">{{ $post->disapproved_status }}</span>
                                @elseif($post->isPending())
                                    <div class="badge rounded-pill bg-warning">
                                        Pending
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="mt-5">
        {{ $posts->links('vendor.pagination.bootstrap-4') }}
    </div>
@endsection
