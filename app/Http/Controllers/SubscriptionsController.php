<?php

namespace App\Http\Controllers;

use App\Http\Requests\Subscriptions\CreateSubscriptionRequest;
use App\Models\Plan;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubscriptionsController extends Controller
{
    //

    public function index()
    {
        $subscriptions = Subscription::all();
        return view('subscriptions.index', compact(['subscriptions']));
    }
    public function add()
    {
        $plans = Plan::all();
        return view('subscriptions.add', compact('plans'));
    }

    public function store(CreateSubscriptionRequest $request)
    {
       // dd($request->plan_id);

        $expires_at = null;
        $plan = Plan::where('id', $request->plan_id)->first();
        if ($plan->isMonthly()) {
            $expires_at = Carbon::now()->addDays(30);
        } else if ($plan->isQuaterly()) {
            $expires_at = Carbon::now()->addDays(90);
        } else if ($plan->isHalfYearly()) {
            $expires_at = Carbon::now()->addDays(180);
        } else if ($plan->isAnnual()) {
            $expires_at = Carbon::now()->addDays(365);
        }

        Subscription::create([
            'user_id' => auth()->id(),
            'plan_id' => $request->plan_id,
            'state' => 'active',
            'start_plan' => now(),
            'expires_at' => $expires_at,
        ]);

        $user = User::where('id', auth()->id())->first();

        $user->update(['role' => User::SUBSCRIBER]);

        session()->flash('success', 'Subscription added successfully');

        return redirect(route('dashboard'));
    }

    public function cancel(Subscription $subscription)
    {
        $subscription->cancel();
        session('success', 'Subscription cancelled successfully');
        return redirect(route('dashboard'));
    }

}
