@extends('layouts.admin-panel.app')

@section('content')
    <div class="card">
        <div class="card-header"><h2>Edit User</h2></div>
        <div class="card-body">
           <form action="{{ route('users.update', $user->id)}}" method="POST">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"
                    class="form-control @error('name') is-invalid @enderror"
                    id="name"
                    value="{{ old('name', $user->name) }}"
                    placeholder="Enter name"
                    name="name">
                    @error('name')
                        <small class="form-text text-danger"> {{ $message}} </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email"
                    class="form-control @error('email') is-invalid @enderror"
                    id="email"
                    value="{{ old('email', $user->email) }}"
                    placeholder="Enter email"
                    name="email">
                    @error('email')
                        <small class="form-text text-danger"> {{ $message}} </small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="name">Role</label>
                    <input type="text"
                    class="form-control @error('role') is-invalid @enderror"
                    id="role"
                    value="{{ old('role', $user->role) }}"
                    placeholder="Enter role"
                    name="role">
                    @error('role')
                        <small class="form-text text-danger"> {{ $message}} </small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-warning">Edit</button>
            </form>
        </div>
    </div>
@endsection

