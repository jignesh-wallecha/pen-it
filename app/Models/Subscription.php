<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $dates = ['start_plan', 'expires_at', 'cancelled_at'];


    /**
     * Relationship methods
     */

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    /**
     * Cancel subscription
     */
    public function cancel(): bool
    {
        $data = [
            'state' => 'cancelled',
            'expires_at' => null,
            'cancelled_at' => now(),
        ];

        return $this->update($data);
    }


}
