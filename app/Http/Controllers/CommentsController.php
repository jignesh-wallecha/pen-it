<?php

namespace App\Http\Controllers;

use App\Http\Requests\Comments\CreateCommentRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function index()
    {
        $comments = Comment::all();
        return view('comments._index', $comments);
    }

    public function store(CreateCommentRequest $request, Post $post)
    {
        $post->comments()->create([
            'comment' => $request->body,
            'user_id' => auth()->id(),
            'post_id' => $post->id,
        ]);

        //TODO: notify owner of the blog that someone has commented on post

        session()->flash('success', 'comment added successfully to' . $post->title);

        return redirect()->back();
    }


    /**
     * This method will return a textbox/textarea to reply to the comment
     */
    public function reply(Post $post, Comment $comment)
    {
        return view('comments.reply', compact(['post', 'comment']));
    }
}
