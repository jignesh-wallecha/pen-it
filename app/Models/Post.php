<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['published_at', 'approved_at', 'disapproved_at']; //carbon

    //Accessor
    public function getImagePathAttribute()
    {
        return 'storage/'.$this->image;
    }

    /**
     * Relationship methods
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * queryscopes
    */
    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now());
    }

    public function scopeDrafted($query)
    {
        return $query->where('published_at', '>', now());
    }

    public function scopeSearch($query): Builder
    {
        $search = request('search');
        if ($search) {
            return $query->where('title', 'LIKE', "%$search%");
        }
        return $query;
    }

    public function scopeApproved($query)
    {
        return $query->where('approved_at', '!=', null);
    }

    public function scopeDisapproved($query)
    {
        return $query->where('disapproved_at', '!=', null);
    }

    /**
     * Custom methods
     */

    public function hasTag(int $tag_id)
    {
        return in_array($tag_id, $this->tags->pluck('id')->toArray());
    }

    public function deleteImage()
    {
        Storage::delete($this->image);
    }

    public function isApproved(): bool
    {
        return isset($this->approved_at) && (!isset($this->disapproved_at));
    }

    public function isDisapproved(): bool
    {
        return isset($this->disapproved_at) && (!isset($this->approved_at));
    }

    public function isPending(): bool
    {
        return !($this->isApproved() && $this->isDisapproved());
    }

}


