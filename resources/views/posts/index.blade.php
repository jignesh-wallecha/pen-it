@extends('layouts.admin-panel.app')

@section('content')
<div class="d-flex justify-content-end mb-3">
    <a href=" {{ route('posts.create') }}" class="btn btn-outline-primary">Add Post</a>
</div>
    <div class="card">
        <div class="card-header"><h2>Posts</h2></div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Excerpt</th>
                    <th>Category</th>
                    <th>Actions</th>
                    @if(! auth()->user()->isAdmin())
                        <th>Status</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td><img src="{{ asset($post->image_path) }}" alt="" width="120"></td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->excerpt }}</td>
                            <td>{{ $post->category->name}}</td>
                            <td>
                                <a href=" {{route('posts.edit', $post->id) }}" class="btn btn-sm btn-primary">
                                    Edit
                                </a>
                                <button type="button" class="btn btn-sm btn-danger mt-2" onclick="displayModal({{ $post->id }})" data-toggle="modal" data-target="#deleteModal">
                                    Trash
                                </button>
                                {{-- @if(auth()->user()->isAdmin())
                                    <script>
                                        // document.querySelectorAll(".hideForAdmin").forEach(function($ele){
                                        //     $ele.style.display = "none";
                                        // });
                                        document.querySelectorAll(".hideForAdmin").forEach($ele => $ele.style.display = "none");
                                    </script> --}}
                                    @if(! $post->author->isAdmin())
                                        <form action="{{ route('posts.approve', $post->id)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                        <button type="submit" class="btn btn-sm btn-warning mt-2 hideForAdmin">Approve</button>
                                        </form>
                                        <button type="button" class="btn btn-sm btn-danger mt-2 hideForAdmin" onclick = "displayDisapproveModal({{ $post->id }})"data-toggle="modal" data-target="#exampleModal">
                                            Disapprove
                                        </button>
                                    @endif


                                @if(auth()->user()->isAdmin())
                                    <a href=" {{ route('posts.preview', $post->id) }}" class="btn btn-sm btn-info mt-2">
                                        Preview
                                    </a>
                                @endif
                            </td>
                            <td>
                                @if(! (auth()->user()->isAdmin()))
                                    @if($post->isApproved())
                                        <div class="badge rounded-pill bg-primary">
                                            Approved
                                        </div>
                                    @elseif($post->isDisapproved())
                                        <div class="badge rounded-pill bg-danger">
                                            Disapproved
                                        </div>
                                        <span class="text-danger">{{ $post->disapproved_status }}</span>
                                    @elseif($post->isPending())
                                        <div class="badge rounded-pill bg-warning">
                                            Pending
                                        </div>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deleteModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="" method="POST" id="deletePostForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    Are u sure, you want to delete this post?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-danger" >Delete Post</button>
                </div>
            </form>
          </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="" method="POST" id="disapprovePutForm">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <label for="message" class="col-form-label">Message:</label>
                    <textarea class="form-control" id="message" name="message"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </form>
          </div>
        </div>
    </div>




    <div class="mt-5">
        {{ $posts->links('vendor.pagination.bootstrap-4') }}
    </div>
@endsection

@section('page-level-scripts')
    <script>
        //alert('hello');
        function displayModal(postId) {
            var url = "/posts/" + postId + "/trash";
            $("#deletePostForm").attr('action', url);
        }

        function displayDisapproveModal(postId) {
            var url = "/posts/" + postId + "/disapprove";
            //console.log(url);
            $("#disapprovePutForm").attr('action', url);
        }

    </script>
@endsection
