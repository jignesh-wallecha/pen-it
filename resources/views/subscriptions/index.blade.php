@extends('layouts.admin-panel.app')

@section('content')

<div class="card">
    <div class="card-header"><h2>Subscriptions</h2></div>
    <div class="card-body">
        <table class="table">
            <thead>
            <tr>
                <th>User</th>
                <th>Plan</th>
                <th>State</th>
                <th>Start Plan</th>
                <th>Expires</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($subscriptions as $subscription)
                    <tr>
                        <td> {{ $subscription->user->name }}</td>
                        <td> {{ $subscription->plan->name }}</td>
                        <td> {{ $subscription->state }}</td>
                        <td> {{ $subscription->start_plan->diffForHumans() }}</td>
                        <td> {{ $subscription->expires_at->diffForHumans() }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection
