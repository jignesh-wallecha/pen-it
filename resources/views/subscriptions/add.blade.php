@extends('layouts.admin-panel.app')

@section('content')
<div class="card">
    <div class="card-header"><h2>Get Your Subscription</h2></div>
    <div class="card-body">
        <form action="{{ route('subscriptions.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="plan">Plan</label>
                <select name="plan_id" id="plan_id" class="form-control" select2>
                    <option value="" disabled selected>Select a Plan</option>
                    {{-- <option></option> --}}
                    @foreach($plans as $plan)
                        <option value="{{$plan->id}}" @if(old('plan_id') == $plan->id) selected @endif>{{ $plan->name}}</option>
                    @endforeach
                </select>
                @error('plan_id')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Subscribe</button>
        </form>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function(){
        $('.select2').select2({
            placeholder: 'Select a plan',
            allowClear: true,
        });
    });

</script>
@endsection
