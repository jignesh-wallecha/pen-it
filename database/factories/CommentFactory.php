<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'comment' => $this->faker->paragraphs(rand(1,2), true),
            'user_id' => User::pluck('id')->random(),
            'post_id' => Post::pluck('id')->random(),
            'parent_id' => Comment::pluck('id')->random(),
        ];

    }
}
