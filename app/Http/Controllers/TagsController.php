<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tags\CreateTagRequest;
use App\Http\Requests\Tags\UpdateTagRequest;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin'])->only('edit', 'update', 'trash', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::paginate(3);
        return view('tags.index', compact(['tags']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request)
    {
        Tag::create([
            'name' => $request->name
        ]);

        //session
        session()->flash('success', 'Tags created successfully');

        return redirect(route('tags.index'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact(['tag']));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        //update tag name
        $tag->update(['name' => $request->name]);

        //session
        session()->flash('success', 'Tag updated successfully');

        //redirect
        return redirect(route('tags.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $tagId)
    {
        $tag = Tag::onlyTrashed()->findOrFail($tagId);

        if ($tag->posts()->count() > 0) {
            session()->flash('error', 'This category cannot be deleted as it is associated with some post');
            return redirect(route('tags.index'));
        }
        $tag->forceDelete();
        session()->flash('success', $tag->name . ' tag deleted successfully');
        return redirect(route('tag.index'));
    }

    public function trashed()
    {
        $trashed = Tag::onlyTrashed()->paginate(10);
        return view('tags.trashed', ['tags' => $trashed]);
    }

    public function trash(Tag $tag)
    {
        //$tag = Tag::findOrFail($id);
        $tag->delete();
        session()->flash('success', $tag->name . ' Tag Trashed');
        return redirect(route('tags.index'));
    }

    public function restore(int $tagId)
    {
        //dd($tagId);
        $tag = Tag::onlyTrashed()->findOrFail($tagId);
        $tag->restore();
        session()->flash('success', 'Tags Restored Successfully');
        return redirect(route('tags.index'));
    }
}
