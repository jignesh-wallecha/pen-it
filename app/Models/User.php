<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    const ADMIN = 'admin';
    const AUTHOR = 'author';
    const SUBSCRIBER = 'subscriber';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Relationship methods
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    //accessor
    public function getGravatarImageAttribute(): string
    {
        return Gravatar::src($this->email, 80);
    }

    /**
     * Custom methods
     */
    public function isAdmin(): bool
    {
        return $this->role === self::ADMIN;
    }

    public function isSubscribed(): bool
    {
        return $this->role === self::SUBSCRIBER;
    }
}
