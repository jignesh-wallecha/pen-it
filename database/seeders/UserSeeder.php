<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ved Sharma',
            'email' => 'ved@mail.com',
            'password' => Hash::make('ved1234'),
            'role' => 'author'
        ]);

        User::create([
            'name' => 'Jerry',
            'email' => 'jerry@mail.com',
            'password' => Hash::make('jerry1234'),
            'role' => 'admin',
        ]);
        User::create([
            'name' => 'Tara',
            'email' => 'tara@mail.com',
            'password' => Hash::make('tara1234'),
            'role' => 'admin',
        ]);
        User::create([
            'name' => 'Heer',
            'email' => 'heer@mail.com',
            'password' => Hash::make('heer1234'),
            'role' => 'author',
        ]);

        User::create([
            'name' => 'Dishant',
            'email' => 'dishant@mail.com',
            'password' => Hash::make('dishant1234'),
            'role' => 'author',
        ]);

        User::create([
            'name' => 'Gaurav',
            'email' => 'gaurav@mail.com',
            'password' => Hash::make('gaurav1234'),
            'role' => 'author',
        ]);



    }
}
